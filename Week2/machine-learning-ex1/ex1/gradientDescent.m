function [theta, J_history] = gradientDescent(X, y, theta, alpha, num_iters)
%GRADIENTDESCENT Performs gradient descent to learn theta
%   theta = GRADIENTDESENT(X, y, theta, alpha, num_iters) updates theta by 
%   taking num_iters gradient steps with learning rate alpha

% Initialize some useful values
m = length(y); % number of training examples
J_history = zeros(num_iters, 1);

for iter = 1:num_iters

    % ====================== YOUR CODE HERE ======================
    % Instructions: Perform a single gradient step on the parameter vector
    %               theta. 
    %
    % Hint: While debugging, it can be useful to print out the values
    %       of the cost function (computeCost) and gradient here.
    %

    mySum0 = 0;
    for i = 1:m
        x = X(i,1:2);
        x = x';
        h = theta' * x; 
        delta = h - y(i);
        mySum0 = mySum0 + delta;
    end % end of sum0 loop
    
    
    mySum1 = 0;
    for i = 1:m
        x = X(i,1:2);
        x = x';
        h = theta' * x; 
        delta = h - y(i);
        delta = delta*x(2);
        mySum1 = mySum1 + delta;
    end % end of sum0 loop
    
    tmp0 = theta(1) - alpha*(1/m)*mySum0;
    tmp1 = theta(2) - alpha*(1/m)*mySum1;
    theta(1) = tmp0;
    theta(2) = tmp1;
    

    % ============================================================

    % Save the cost J in every iteration    
    J_history(iter) = computeCost(X, y, theta);

end

end
