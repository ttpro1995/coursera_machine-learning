function J = computeCost(X, y, theta)
%COMPUTECOST Compute cost for linear regression
%   J = COMPUTECOST(X, y, theta) computes the cost of using theta as the
%   parameter for linear regression to fit the data points in X and y

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta
%               You should set J to the cost.

% X = [1 sth; 1 sth; 1 sth ....] 
% Y = [column vector]
% theta = [sth; sth]
mySum = 0;
for i = 1:m
    x = X(i,1:2);
    x = x';
    h = theta' * x; 
    delta = h - y(i);
    delta = delta^2;
    mySum = mySum + delta;
end % end of sum loop
J = (1/(2*m))*mySum; % return

% =========================================================================
