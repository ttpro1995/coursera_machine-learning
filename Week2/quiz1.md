1. 0.72
2. You run gradient descent for 15 iterations with α=0.3 and compute J(θ) after each iteration. You find that the value of J(θ) decreases quickly then levels off. Based on this, which of the following conclusions seems most plausible? <br>
Rather than use the current value of α, it'd be more promising to try a smaller value of α (say α=0.1).<br>
3. Suppose you have m=28 training examples with n=4 features (excluding the additional all-ones feature for the intercept term, which you should add). The normal equation is θ=(XTX)−1XTy. For the given values of m and n, what are the dimensions of θ, X, and y in this equation? <br>
X is 28×4, y is 28×1, θ is4×1 <br>
4. Suppose you have a dataset with m=50 examples and n=200000 features for each example. You want to use multivariate linear regression to fit the parameters θ to our data. Should you prefer gradient descent or the normal equation? <br>
The normal equation, since it provides an efficient way to directly find the solution. <br>
5. Which of the following are reasons for using feature scaling?<br>
It speeds up gradient descent by making it require fewer iterations to get to a good solution. <br>
It speeds up gradient descent by making each iteration of gradient descent less expensive to compute. <br>

----------------
1. 0.3 <br>
2. use smaller 0.1 <br>
3. m=23 training examples with n=5 <br>
X is 23×6, y is 23×1, θ is 6×1 <br>
4. m=50 examples and n=200000 <br>
Gradient descent, since (XTX)−1 will be very slow to compute in the normal equation. <br>
5. Which of the following are reasons for using feature scaling?<br>
It speeds up gradient descent by making it require fewer iterations to get to a good solution.
It speeds up solving for θ using the normal equation.
--------
2. decrease and level of => optimize
3.
4. 
