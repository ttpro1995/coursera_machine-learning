1.

2. uppose a massive dataset is available for training a learning algorithm. Training on a lot of data is likely to give good performance when two of the following conditions hold true. <br>
The features x contain sufficient information to predict y accurately. (For example, one way to verify this is if a human expert on the domain can confidently predict y when given only x).<br>
We train a learning algorithm with a large number of parameters (that is able to learn/represent fairly complex functions). <br>
We train a model that does not use regularization. <br>
3. Increase threshold
The classifier is likely to now have higher precision.

4. Suppose you are working on a spam classifier, where spam emails are positive examples (y=1) and non-spam emails are negative examples (y=0). You have a training set of emails in which 99% of the emails are non-spam and the other 1% is spam. Which of the following statements are true? Check all that apply. <br>
y = 0 => acc = 99<br>
y = 1 => recall = 100, precision = 1<br>
y = 0 => recall  = 0;<br>

5.
Using a very large training set makes it unlikely for model to overfit the training data.<br>
The "error analysis" process of manually examining the examples which your algorithm got wrong can help suggest what are good steps to take (e.g., developing new features) to improve your algorithm's performance.<br>
