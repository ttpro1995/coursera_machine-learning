tp = 85;
fp = 890;

fn = 15;
tn = 10;

precision = tp/(tp+fp);
recall = tp/(tp+fn);

f1 = (2*precision*recall)/(precision+recall);
accuracy = (tp+tn)/(tp+fp+fn+tn);
accuracy
