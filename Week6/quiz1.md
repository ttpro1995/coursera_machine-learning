2. Suppose you have implemented regularized logistic regression to classify what object is in an image (i.e., to do object recognition). However, when you test your hypothesis on a new set of images, you find that it makes unacceptably large errors with its predictions on the new images. However, your hypothesis performs well (has low error) on the training set. Which of the following are promising steps to take? Check all that apply.
Get more training examples.
Try using a smaller set of features.

3. Suppose you have implemented regularized logistic regression to predict what items customers will purchase on a web shopping site. However, when you test your hypothesis on a new set of customers, you find that it makes unacceptably large errors in its predictions. Furthermore, the hypothesis performs poorly on the training set. Which of the following might be promising steps to take? Check all that apply.<br>
Try adding polynomial features.
Try to obtain and use additional features.


4. Which of the following statements are true? Check all that apply. <br>
A typical split of a dataset into training, validation and test sets might be 60% training set, 20% validation set, and 20% test set.
The performance of a learning algorithm on the training set will typically be better than its performance on the test set.
Suppose you are training a regularized linear regression model. The recommended way to choose what value of regularization parameter λ to use is to choose the value of λ which gives the lowest cross validation error.

5. Which of the following statements are true? Check all that apply.
A model with more parameters is more prone to overfitting and typically has higher variance.
If a learning algorithm is suffering from high bias, only adding more training examples may not improve the test error significantly.
When debugging learning algorithms, it is useful to plot a learning curve to understand if there is a high bias or high variance problem.
