m = 4;
x = load('xdata.dat')
y = load('ydata.dat')

figure % open a new figure window
plot(x,y,'o');
xlabel('x axis');
ylabel('y axis');

m = length(y); % number of training example
% x = [ones(m,1),x]; % add column of ones to x

alpha = 0.07; % learning rate
t0 = zeros(m,1);
t1 = zeros(m,1);

h = t1 .* x;

aaa = h - y;
test =  alpha .* (1/m).*sum((h - y).*x);
for i = 1:100
    h = t0 + t1 .* x;
    tmp0 = t0 - alpha .* (1/m)*sum((h - y));
    tmp1 = t1 - alpha .* (1/m)*sum((h - y).*x);
    t0 = tmp0;
    t1 = tmp1;
end

t0
t1

%--------------