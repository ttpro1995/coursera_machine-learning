1 .A computer program is said to learn from experience E with respect to some task T and some performance measure P if its performance on T, as measured by P, improves with experience E. Suppose we feed a learning algorithm a lot of historical weather data, and have it learn to predict weather. What would be a reasonable choice for P?
The probability of it correctly predicting a future date's weather.<br>

2. Suppose you are working on weather prediction, and your weather station makes one of three predictions for each day's weather: Sunny, Cloudy or Rainy. You'd like to use a learning algorithm to predict tomorrow's weather. Would you treat this as a classification or a regression problem? <br>
Classification<br>

3. Suppose you are working on stock market prediction. You would like to predict whether or not a certain company will declare bankruptcy within the next 7 days (by training on data of similar companies that had previously been at risk of bankruptcy). Would you treat this as a classification or a regression problem? <br>
Classification <br>

4. Some of the problems below are best addressed using a supervised learning algorithm, and the others with an unsupervised learning algorithm. Which of the following would you apply supervised learning to? (Select all that apply.) In each case, assume some appropriate dataset is available for your algorithm to learn from. <r>
In farming, given data on crop yields over the last 50 years, learn to predict next year's crop yields. <br>
Examine a web page, and classify whether the content on the web page should be considered "child friendly" (e.g., non-pornographic, etc.) or "adult."<br>

5.  Which of these is a reasonable definition of machine learning? <br>
Machine learning is the field of study that gives computers the ability to learn without being explicitly programmed.<br>


Supervised: given right answer (regression).
classification: discrete value

unsupervised: given data no label, find the structure of data.
(clustering)
