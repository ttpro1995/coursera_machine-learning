1.  Consider the problem of predicting how well a student does in her second year of college/university, given how well they did in their first year.<br>
4<br>

2. Consider the following training set of m=4 training examples:<br>
 What are the values of θ0 and θ1 that you would expect to obtain upon running gradient descent on this model? (Linear regression will be able to fit this data perfectly.) <br>
 0, 0.5<br>

 3. Suppose we set θ0=−1,θ1=2 in the linear regression hypothesis from Q1. What is hθ(6)? <br>
 11

 4. Let f be some function so that f(θ0,θ1) outputs a number. For this problem, f is some arbitrary/unknown smooth function (not necessarily the cost function of linear regression, so f may have local optima). Suppose we use gradient descent to try to minimize f(θ0,θ1) <br> X
Even if the learning rate α is very large, every iteration of gradient descent will decrease the value of f(θ0,θ1).
If the learning rate is too small, then gradient descent may take a very long time to converge. <br>
If θ0 and θ1 are initialized at a local minimum, then one iteration will not change their values. <br>

5.  Suppose that for some linear regression problem (say, predicting housing prices as in the lecture), we have some training set, and for our training set we managed to find some θ0, θ1 such that J(θ0,θ1)=0.<br> C

Which of the statements below must then be true? (Check all that apply.)<br>

Our training set can be fit perfectly by a straight line, <br>
For this to be true, we must have y(i)=0 for every value of i=1,2,…,m. <br>
For this to be true, we must have θ0=0 and θ1=0 so that hθ(x)=0 <br>
