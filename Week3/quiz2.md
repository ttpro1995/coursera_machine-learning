1. You are training a classification model with logistic regression. Which of the following statements are true? Check all that apply. <br>
Adding a new feature to the model always results in equal or better performance on the training set.

2. Suppose you ran logistic regression twice, once with λ=0, and once with λ=1. One of the times, you got parameters θ and the other time you got θ. However, you forgot which value of λ corresponds to which value of θ. Which one do you think corresponds to λ=1? <br>
small one <br>

3. Which of the following statements about regularization are true? Check all that apply. <br>
Using too large a value of λ can cause your hypothesis to underfit the data.

4. In which one of the following figures do you think the hypothesis has overfit the training set? <br>
all X in line

5. In which one of the following figures do you think the hypothesis has underfit the training set?
some X out of line
