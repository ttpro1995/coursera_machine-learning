function [J, grad] = costFunction(theta, X, Y)
%COSTFUNCTION Compute cost and gradient for logistic regression
%   J = COSTFUNCTION(theta, X, y) computes the cost of using theta as the
%   parameter for logistic regression and the gradient of the cost
%   w.r.t. to the parameters.

% Initialize some useful values
m = length(Y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta
%
% Note: grad should have the same dimensions as theta
%
% 


% use . instead of for loop
z = X * theta ; % row vector of z for each training data
h = sigmoid(z); % row vector of h
sum1 = sum(-Y.*log(h)); 
sum2 = sum((1-Y).*log(1-h));
totalSum = sum1-sum2; % the sigma part 
J = totalSum/m;




% gSum1 = sum((h-Y).*X(:,1)); 
% gSum2 = sum((h-Y).*X(:,2));
% gSum3 = sum((h-Y).*X(:,3));
% grad(1) = gSum1/m;
% grad(2) = gSum2/m;
% grad(3) = gSum3/m;

% same with the code above
gSum = (h-Y)'*X;
grad = gSum/m;


% =============================================================

end
