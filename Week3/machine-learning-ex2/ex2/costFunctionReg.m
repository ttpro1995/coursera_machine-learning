function [J, grad] = costFunctionReg(theta, X, Y, lambda)
%COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(Y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta




% use . instead of for loop
z = X * theta ; % row vector of z for each training data
h = sigmoid(z); % row vector of h
sum1 = sum(-Y.*log(h)); 
sum2 = sum((1-Y).*log(1-h));
totalSum = sum1-sum2; % the sigma part 

regu = sum(theta.*theta) - theta(1)*theta(1);
regu = (lambda/(2*m))*regu;


J = totalSum/m + regu;


rRegu = (lambda/m)*theta;
rRegu(1) = 0;
gSum = (h-Y)'*X;
grad = gSum/m;
grad = grad' + rRegu;

% =============================================================

end
