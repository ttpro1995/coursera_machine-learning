1. Which of the following statements are true? Check all that apply. <br>
The activation values of the hidden units in a neural network, with the sigmoid activation function applied at every layer, are always in the range (0, 1).
Any logical function over binary-valued (0 or 1) inputs x1 and x2 can be (approximately) represented using some neural network.

In a neural network with many layers, we think of each successive layer as being able to use the earlier layers as features, so as to be able to compute increasingly complex functions. <br>
If a neural network is overfitting the data, one solution would be to increase the regularization parameter λ. <br>

2. Consider the following neural network which takes two binary-valued inputs x1,x2∈{0,1} and outputs hΘ(x). Which of the following logical functions does it (approximately) compute? <br>
30 -20 -20  
NAND

3. Consider the neural network given below. Which of the following equations correctly computes the activation a(3)1? Note: g(z) is the sigmoid activation function. <br>
X a(3)1=g(Θ(1)1,0a(2)0+Θ(1)1,1a(2)1+Θ(1)1,2a(2)2) <br>
a(3)1=g(Θ(2)1,0a(2)0+Θ(2)1,1a(2)1+Θ(2)1,2a(2)2)


4. You'd like to compute the activations of the hidden layer a(2)∈R3. One way to do so is the following Octave code: <br>
a2 = sigmoid (Theta1 * x); <br>

5. It will stay the same.
