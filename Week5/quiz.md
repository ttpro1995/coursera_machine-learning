1. Δ(2):=Δ(2)+δ(3)∗(a(2))T
2. reshape(thetaVec(16:39),4,6)
4. Which of the following statements are true? Check all that apply. <br>
---------------------
Using gradient checking can help verify if one's implementation of backpropagation is bug-free.<br>
For computational efficiency, after we have performed gradient checking to verify that our backpropagation code is correct, we usually disable gradient checking before using backpropagation to train the network. <br>


5. Which of the following statements are true? Check all that apply.

------------
Suppose you are training a neural network using gradient descent. Depending on your random initialization, your algorithm may converge to different local optima (i.e., if you run the algorithm twice with different random initializations, gradient descent may converge to two different solutions). <br>

If we are training a neural network using gradient descent, one reasonable "debugging" step to make sure it is working is to plot J(Θ) as a function of the number of iterations, and make sure it is decreasing (or at least non-increasing) after each iteration.<br>
