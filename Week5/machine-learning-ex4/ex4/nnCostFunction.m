function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% Setup some useful variables
m = size(X, 1); % m = 5000
K = num_labels; % K = 10
         
% You need to return the following variables correctly 
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

% ====================== YOUR CODE HERE ======================
% Instructions: You should complete the code by working through the
%               following parts.
%
% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m

% transform y 5000x1 to y 5000x10
tmy = zeros(m,K);
for i=1:K
    tmy(:,i) = (y==i);
end
y = tmy;
% y 5000x10   

% Theta1 25x401
% Theta2 10x26
% X 5000x400
% X 5000x401

% calculate h(x)_k 
X = [ones(m,1) X]; % add column 1
% X 5000x401 

z2 = X*Theta1'; % z2= 5000x401 * 401*25 = 5000x25  
a2 = sigmoid(z2);% a2 5000x25
a2 = [ones(m,1) a2]; % a2 5000x26 
z3 = a2*Theta2'; % z3 = 5000x26 * 26x10 = 5000x10
a3 = sigmoid(z3); % a3 5000x10
h = a3; % h 5000x10

% calculate the part in sigma
tmp = -y.*log(h) - (1-y).*log(1 - h); % tmp 5000x10
tmp = sum(tmp,2); % sigma k
tmp = sum(tmp,1); % sigma m
J = tmp/m;

% calculate Regularized part
sqrTheta1 = Theta1.*Theta1; % sqrTheta1 = 25x401
sqrTheta1(:,1) = []; % sqrTheta1 25x400
sumTheta1 = sum(sqrTheta1,2);
sumTheta1 = sum(sumTheta1);

sqrTheta2 = Theta2.*Theta2; % sqrTheta2 = 10x26
sqrTheta2(:,1) = []; % sqrTheta2 = 10x25
sumTheta2 = sum(sqrTheta2,2);
sumTheta2 = sum(sumTheta2);
regularized = (lambda/(2*m))*(sumTheta1+sumTheta2);

J = J + regularized; % cost function

%
% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients
%
%         Note: The vector y passed into the function is a vector of labels
%               containing values from 1..K. You need to map this vector into a 
%               binary vector of 1's and 0's to be used with the neural network
%               cost function.
%
%         Hint: We recommend implementing backpropagation using a for-loop
%               over the training examples if you are implementing it for the 
%               first time.

delta3 = a3 - y; % delta3 5000x10
%Theta2 10x26
%delta3 5000x10
%z2 5000x25
z2 = [ones(m,1) z2]; % z2 5000x26
delta2 = delta3*Theta2.*sigmoidGradient(z2);
%delta2 5000x26
delta2(:,1)=[]; %remove delta20
%delta2 5000x25

Theta1_grad = delta2'*X; 
%Theta1_grad = (25x5000)*(5000x401) = 25x401
Theta1_grad = Theta1_grad/m;


Theta2_grad = delta3'*a2;
%Theta2_grad = (10x5000)*(5000x26) = 10x26
Theta2_grad = Theta2_grad/m;


%
% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.
%

Theta1_reg = (lambda/m)*Theta1;
Theta1_reg(:,1) = zeros(size(Theta1_reg,1),1);

Theta2_reg = (lambda/m)*Theta2;
Theta2_reg(:,1) = zeros(size(Theta2_reg,1),1);

Theta1_grad = Theta1_grad+ Theta1_reg;
Theta2_grad = Theta2_grad+ Theta2_reg;















% -------------------------------------------------------------

% =========================================================================

% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];


end
