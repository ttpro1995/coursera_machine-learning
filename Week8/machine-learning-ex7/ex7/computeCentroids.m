function centroids = computeCentroids(X, idx, K)
%COMPUTECENTROIDS returs the new centroids by computing the means of the 
%data points assigned to each centroid.
%   centroids = COMPUTECENTROIDS(X, idx, K) returns the new centroids by 
%   computing the means of the data points assigned to each centroid. It is
%   given a dataset X where each row is a single data point, a vector
%   idx of centroid assignments (i.e. each entry in range [1..K]) for each
%   example, and K, the number of centroids. You should return a matrix
%   centroids, where each row of centroids is the mean of the data points
%   assigned to it.
%

% Useful variables
[m n] = size(X);
% m number of example point
% n number of example dimension

% You need to return the following variables correctly.
centroids = zeros(K, n);


% ====================== YOUR CODE HERE ======================
% Instructions: Go over every centroid and compute mean of all points that
%               belong to it. Concretely, the row vector centroids(i, :)
%               should contain the mean of the data points assigned to
%               centroid i.
%
% Note: You can use a for-loop over the centroids to compute this.
%

for i = 1:K % run loop for each centroid
    sum = zeros(1,n); % a sum vector of all data point assign to c_i
    count = 0;
    for j = 1:m % run loop for each data point
        if idx(j) == i % if j data point is assign to c_i
            sum = sum + X(j,:); % add data point to sum
            count = count + 1; % counting data point
        end
    end
    sum = sum/count; % divide each feature of sum by count 
    centroids(i,:) = sum;
end







% =============================================================


end

