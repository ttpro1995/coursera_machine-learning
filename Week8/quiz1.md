1. For which of the following tasks might K-means clustering be a suitable algorithm? Select all that apply. <br>
Given a database of information about your users, automatically group them into different market segments.
Given sales data from a large number of products in a supermarket, figure out which products tend to form coherent groups (say are frequently purchased together) and thus should be put on the same shelf.

3. K-means is an iterative algorithm, and two of the following steps are repeatedly carried out in its inner-loop. Which two?
The cluster assignment step, where the parameters c(i) are updated.
Move the cluster centroids, where the centroids μk are updated.

4. Suppose you have an unlabeled dataset {x(1),…,x(m)}. You run K-means with 50 different random
For each of the clusterings, compute 1m∑mi=1||x(i)−μc(i)||2, and pick the one that minimizes this.

5. Which of the following statements are true? Select all that apply.
On every iteration of K-means, the cost function J(c(1),…,c(m),μ1,…,μk) (the distortion function) should either stay the same or decrease; in particular, it should not increase..
A good way to initialize K-means is to select K (distinct) examples from the training set and set the cluster centroids equal to these selected examples.
